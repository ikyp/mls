# `MLS`

## **Intro**

This repo is a little status _program_ called **mls**.

This program use on `Windown Manager`.

It's has real needed feature for you.

It's simple and clear.

## **Usage**

``` Makefile
make
make install
```

_Installed to `/user/local/bin`_

Then, add `lstatus &` to your `.xprofile` file or `.xinitrc` file

## **Screenshots**

![alt text](https://bytebucket.org/ikyp/mls/raw/f1d5de726e91e5c2a2da4212b5615a20081bb615/statusbar.png)